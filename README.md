# Python Books

## About
Python Library is an demo application created by Andrew Jorgenson in django.
Styling and responsiveness are time-intensive to achieve. This demo application was only built to be functional. To see an example of perfectionism, please check out my [portfolio](https://www.andrewjorgenson.net/) built with Vue3 and TailwindCSS hosted via Netlify for CI/CD.


**Your feedback would be greatly appreciated!**

## Setup
**Create a virtual environment**

`pip -m venv venv`

**Activate virtual environment**

`source venv/bin/activate`

**Install dependencies**

`pip install -r requirements.txt`

**Ensure latest migrations have been ran**

`python manage.py migrate`

**Run tests**

`python manage.py test`

**Run server**

`python manage.py runserver`

Additional configuration may be adjusted in [settings.py](books/settings.py)

## Features
### Back-End

* Django
    * Fairly new to the framework and haven't had the opportunity to 
    collaborate with other developers on it.
    * If you spot something silly, please let me know!
* Okta Authentication
    * Chose to go with strictly token validation. This mimics a real world application where microservices are tied together by an SPA.
    Initially tried Auth0. Their documentation had several different examples. One had deprecated packages, the others had the back-end distribute the token to the client - rather than the client retrieving the token from the credential provider directly.
    When going to use Okta, the okta-jwt package was unable to retrieve the rotating keys correctly. I found an open issue in the git repo for this.
    * Since the package was not going to work and I already had invested far more time than I'd like to into auth for a demo app, I chose to hit Okta's API to introspect the token. This is likely a no-no from a performance perspective; however, I wonder how bad it really is considering you have to hit Okta for their rotating public keys anyways when validating the token.
* PyTest unit tests
* Integration with Google Books API, custom client

### Front-End
* Vue
* Vue Router
* VueX (State Management/Store)
* TailwindCss
* Okta Authentication

## Areas for design improvement
* Could implement Redis caching on google books search queries to reduce API calls
* okta auth token on front end does not refresh, so when it times out the user has to log out & log back in
* CSRF protection needs to be implemented
* Would be nice to wrap the Okta auth into the Django properly
* allow pagination of books
* validation via serializers
* change determination of books being in a user's library to being appended * on search results rather than being done by the front end
* database is not fully normalized, unnecessary fields are kept for expandability as no strict requirements have been set
* APIs could return only the necessary data to the front end
* Linting is fairly unrestrictive on the front end right now
* CORS policy is unrestrictive (other than what's managed by Okta)