from django.core.exceptions import SuspiciousOperation


class UnableToAuthenticate(SuspiciousOperation):
    pass
