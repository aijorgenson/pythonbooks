from books.models import Book
from django.conf import settings
import json
import pytest


@pytest.mark.django_db
def test_it_creates_a_book_and_dependencies_successfully_from_google_api_result():
    volume = json.loads(open(
        'books/tests/mock_responses/google_books_volume.json', 'r').read())
    # normally this would not be necessary, but we're having to work with the existing database due to poor django pytest support
    try:
        book_to_delete = Book.objects.get(id=volume['id'])
        book_to_delete.delete()
    except Book.DoesNotExist:
        pass

    book = Book.create_from_google_books_api(volume)
    assert book.title == volume['volumeInfo']['title']
    assert book.published_date == volume['volumeInfo']['publishedDate']
    assert book.publisher.name == volume['volumeInfo']['publisher']
    assert list(map(lambda b: b['name'], book.authors.values(
        'name'))) == volume['volumeInfo']['authors']
    assert book.image_links == volume['volumeInfo']['imageLinks']
