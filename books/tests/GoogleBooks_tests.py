from books.clients.googlebooks import GoogleBooks
import requests
import json
from django.conf import settings
from urllib.parse import urlencode
from books.tests.utils import mock_google_books_search, get_mock_response
# Note - I'm aware that if no parameters are passed into requests_mock that it acts as a wildcard.
# I wanted query parameters to be tested as well.


def test_it_returns_the_correct_data_for_a_single_volume(requests_mock):
    volume_response = get_mock_response('google_books_volume.json')
    parameters = {
        'apiKey': settings.GOOGLE_BOOKS_API_KEY,
    }
    requests_mock.get("https://www.googleapis.com/books/v1/volumes/%s?%s" %
                      ('HelloWorld', urlencode(parameters)), text=volume_response)
    google_books = GoogleBooks()
    volume = google_books.volume('HelloWorld')
    assert json.loads(volume_response) == volume


def test_it_returns_the_correct_data_for_a_search(requests_mock):
    mock_google_books_search(requests_mock, 'testing')
    google_books = GoogleBooks()
    volumes = google_books.volumes('testing')
    assert get_mock_response(
        'google_books_volume_search.json', json_decode=True) == volumes


def test_error_response_for_multiple_volumes_throws_exception(requests_mock):
    parameters = {
        'apiKey': settings.GOOGLE_BOOKS_API_KEY,
        'q': 'testing',
        'maxResults': 20,
        'startIndex': 0,
        'projection': 'lite'
    }
    requests_mock.get("https://www.googleapis.com/books/v1/volumes?%s" %
                      urlencode(parameters), text="an error", status_code=422)
    google_books = GoogleBooks()

    # I initially tried to be all elegant with pytest.raises but with no luck!
    # I'm sure I could get it working with enough time.
    exception_thrown = False
    try:
        volumes = google_books.volumes('testing')
    except requests.exceptions.HTTPError:
        exception_thrown = True
    assert exception_thrown


def test_error_response_for_single_volume_throws_exception(requests_mock):
    parameters = {
        'apiKey': settings.GOOGLE_BOOKS_API_KEY,
    }
    requests_mock.get("https://www.googleapis.com/books/v1/volumes/%s?%s" %
                      ('HelloWorld', urlencode(parameters)), text='an error', status_code=404)
    google_books = GoogleBooks()
    exception_thrown = False
    try:
        volume = google_books.volume('HelloWorld')
    except requests.exceptions.HTTPError:
        exception_thrown = True
    assert exception_thrown
