from django.conf import settings
from django.test import Client
from books.models import Book, OktaUser
from urllib.parse import urlencode
from typing import Union
import json
import requests_mock


def get_mock_response(subpath: str, json_decode=False) -> Union[str, dict]:
    response = open('books/tests/mock_responses/%s' % subpath, 'r').read()
    if json_decode:
        return json.loads(response)
    return response


def mock_authentication(requests_mock):
    # For some reason manually creating a requests mock adapter within the method doesn't appear to work :(
    # Ended up having to require dynamic dependency injection from pytest
    okta_introspect = get_mock_response('okta_active.json')
    requests_mock.register_uri('POST', "%s/oauth2/v1/introspect" %
                               settings.OKTA_AUTH_ISSUER, text=okta_introspect)


def get_test_user():
    okta_introspect = get_mock_response('okta_active.json', json_decode=True)
    user, _ = OktaUser.objects.get_or_create(
        uid=okta_introspect['uid'], username=okta_introspect['username'])
    return user


def make_book_available():
    volume = get_mock_response('google_books_volume.json', json_decode=True)
    try:
        return Book.objects.get(id=volume['id'])
    except Book.DoesNotExist:
        return Book.create_from_google_books_api(volume)


def mock_google_books_search(requests_mock, query: str):
    volume_search_response = get_mock_response(
        'google_books_volume_search.json')
    parameters = {
        'apiKey': settings.GOOGLE_BOOKS_API_KEY,
        'q': query,
        'maxResults': 20,
        'startIndex': 0,
        'projection': 'lite'
    }
    requests_mock.get("https://www.googleapis.com/books/v1/volumes?%s" %
                      urlencode(parameters), text=volume_search_response)


BEARER_TOKEN_MESSAGE = 'Bearer can be anything since the okta introspect request is intercepted by mock_authentication'


def json_post(url: str, data: dict):
    client = Client()
    return client.post(url, data, content_type='application/json', Authorization=BEARER_TOKEN_MESSAGE)


def get(url: str, query: dict):
    client = Client()
    return client.get(url, data=query, Authorization=BEARER_TOKEN_MESSAGE)
