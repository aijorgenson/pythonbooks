import pytest
from books.models import Book, OktaUser
from books.tests.utils import *
from django.conf import settings


@pytest.mark.django_db
def test_book_add(requests_mock):
    book = make_book_available()
    mock_authentication(requests_mock)
    response = json_post('/api/book/add', data={'book_id': book.id})
    assert response.status_code == 201
    user = get_test_user()
    assert user.books.filter(id=book.id).exists()


@pytest.mark.django_db
def test_book_remove(requests_mock):
    book = make_book_available()
    user = get_test_user()
    user.books.add(book)
    mock_authentication(requests_mock)
    response = json_post('/api/book/remove', data={'book_id': book.id})
    assert response.status_code == 204
    assert not user.books.filter(id=book.id).exists()


@pytest.mark.django_db
def test_book_search(requests_mock):
    mock_authentication(requests_mock)
    mock_google_books_search(requests_mock, 'testing')
    response = get('/api/book/search', {'query': 'testing'})
    assert response.status_code == 200
    assert get_mock_response('google_books_volume_search.json',
                             json_decode=True) == json.loads(response.json())
