from books.backends import OktaBackend
from books.tests.utils import get_mock_response
from django.conf import settings
from django.core.exceptions import PermissionDenied
import pytest


class Object(object):
    pass


def get_fake_request(header_auth=False, environ_auth=False):
    request = Object()
    request.headers = {'Authorization': 'some auth'} if header_auth else {}
    request.environ = {'Authorization': 'some auth'} if environ_auth else {}
    return request


def test_that_it_raises_permission_denied_without_authorization_header():
    exception_thrown = False
    try:
        backend = OktaBackend()
        backend.authenticate(get_fake_request())
    except PermissionDenied:
        exception_thrown = True
    assert exception_thrown


@pytest.mark.django_db
def test_that_it_passes_with_bearer_in_headers(requests_mock):
    okta_introspect = get_mock_response('okta_active.json')
    requests_mock.register_uri('POST', "%s/oauth2/v1/introspect" %
                               settings.OKTA_AUTH_ISSUER, text=okta_introspect)
    backend = OktaBackend()
    user = backend.authenticate(get_fake_request(header_auth=True))
    assert user


@pytest.mark.django_db
def test_that_it_passes_with_bearer_in_environ(requests_mock):
    okta_introspect = get_mock_response('okta_active.json')
    requests_mock.register_uri('POST', "%s/oauth2/v1/introspect" %
                               settings.OKTA_AUTH_ISSUER, text=okta_introspect)
    backend = OktaBackend()
    user = backend.authenticate(get_fake_request(environ_auth=True))
    assert user


@pytest.mark.django_db
def test_that_it_fails_with_inactive_authorization_header(requests_mock):
    okta_introspect = get_mock_response('okta_inactive.json')
    requests_mock.register_uri('POST', "%s/oauth2/v1/introspect" %
                               settings.OKTA_AUTH_ISSUER, text=okta_introspect)
    backend = OktaBackend()
    user = backend.authenticate(get_fake_request(header_auth=True))
    assert not user
