from django.contrib.auth.backends import BaseBackend
from django.http import HttpRequest
from django.core.exceptions import PermissionDenied
from books.models import OktaUser
import requests
from django.conf import settings


class OktaBackend(object):
    def authenticate(self, request, username=None, password=None):
        bearer = None
        if 'Authorization' in request.headers:
            bearer = request.headers['Authorization']
        elif 'Authorization' in request.environ:
            bearer = request.environ['Authorization']
        else:
            raise PermissionDenied("No authorization header present")

        access_token = bearer.replace('Bearer ', '')
        response = requests.post("%s/oauth2/v1/introspect" % settings.OKTA_AUTH_ISSUER,
                                 data={
                                     'token_type_hint': 'access_token',
                                     'client_id': settings.OKTA_AUTH_CLIENT_ID,
                                     'token': access_token
                                 },
                                 headers={
                                     'Content-Type': 'application/x-www-form-urlencoded'
                                 })

        if response.status_code != 200:
            return None

        okta_result = response.json()
        if not okta_result['active']:
            print('token not active')
            return None

        user, _ = OktaUser.objects.get_or_create(
            uid=okta_result['uid'], username=okta_result['username'])
        return user
