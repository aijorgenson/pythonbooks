import json
from django.http import HttpResponse, JsonResponse
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate
from django.core import serializers
from books.models import Book
from books.exceptions import UnableToAuthenticate
from books.clients.googlebooks import GoogleBooks

# simple helper since it doesn't seem that you can pass status_code into constructor args for HttpResonse


def response(message: str, code=200) -> HttpResponse:
    response = HttpResponse(message)
    response.status_code = code
    return response


def json_response(data: dict, code=200) -> JsonResponse:
    response = JsonResponse(data, safe=False)
    response.status_code = code
    return response


def service_response(request) -> HttpResponse:
    return response('api is up')


def retrieve_user_or_fail(request):
    user = authenticate(request)
    if user is None:
        raise UnableToAuthenticate("Unable to authenticate")

    return user


def book_search(request):
    # establishing auth as this route does not need to be public
    user = retrieve_user_or_fail(request)
    if 'query' not in request.GET:
        return response('query parameter is required', 400)
    google_books = GoogleBooks()
    return json_response(json.dumps(google_books.volumes(query=request.GET.get('query'))))


def library(request):
    user = retrieve_user_or_fail(request)

    return response(serializers.serialize('json', user.books.all()))


@csrf_exempt
def book_add(request):
    user = retrieve_user_or_fail(request)
    body = json.loads(request.body)
    if 'book_id' not in body:
        return response('book_id is required', 400)
    book = Book.find_or_create(body['book_id'])
    user.books.add(book)
    return response('book added', 201)


@csrf_exempt
def book_remove(request):
    user = retrieve_user_or_fail(request)
    body = json.loads(request.body)
    if 'book_id' not in body:
        return response('book_id is required', 400)
    book = Book.objects.get(pk=body['book_id'])
    user.books.remove(book)
    return response('book removed', 204)
