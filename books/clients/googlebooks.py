import requests
from django.conf import settings


class GoogleBooks:
    def volumes(self, query: str, offset=0, projection='lite') -> list:
        """
        Search for volumes
        :param query:
        :param offset:
        :param projection: Desired volume detail https://developers.google.com/books/docs/v1/using#projection
        :return:
        """
        parameters = {
            'apiKey': self.__get_api_key(self),
            'q': query,
            'maxResults': 20,
            'startIndex': offset,
            'projection': projection
        }
        response = requests.get(
            'https://www.googleapis.com/books/v1/volumes', parameters)
        response.raise_for_status()
        return response.json()

    def volume(self, volume_id: str, projection='lite') -> dict:
        """
        Retrieve an individual volume by id
        :param volume_id:
        :param projection: Desired volume detail https://developers.google.com/books/docs/v1/using#projection
        :return:
        """
        parameters = {
            'apiKey': self.__get_api_key(self),
            'projection': projection
        }
        response = requests.get(
            'https://www.googleapis.com/books/v1/volumes/%s' % volume_id, parameters)
        response.raise_for_status()
        return response.json()

    @staticmethod
    def __get_api_key(self) -> str:
        try:
            return settings.GOOGLE_BOOKS_API_KEY
        except AttributeError:
            raise ValueError('GOOGLE_BOOKS_API_KEY is required. To acquire one, please visit '
                             'https://console.developers.google.com/apis/api/books.googleapis.com/overview')
