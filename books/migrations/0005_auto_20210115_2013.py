# Generated by Django 3.1.5 on 2021-01-15 20:13

import books.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0004_auto_20210115_2001'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='image_links',
            field=models.JSONField(
                default=books.models.Book.image_links_default),
        ),
    ]
