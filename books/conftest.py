from django.conf import settings
import pytest
import os

# sadly this is useless because of https://github.com/pytest-dev/pytest-django/issues/297
# @pytest.fixture(autouse=True)
# def configure_testing_db():
#     BASE_DIR = os.getcwd()
#     settings.DATABASES = {'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': BASE_DIR + '/db_pytest.sqlite3',
#     }}
