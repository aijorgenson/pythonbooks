from django.db import models
from django.contrib.auth.models import AbstractUser
from books.clients.googlebooks import GoogleBooks
import json


class Publisher(models.Model):
    name = models.CharField(max_length=256)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Author(models.Model):
    name = models.CharField(max_length=128)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Book(models.Model):
    # volume id from Google
    id = models.CharField(primary_key=True, max_length=64)
    publisher = models.ForeignKey(
        Publisher, on_delete=models.CASCADE, db_constraint=False)
    # cannot safely cast as date, Google does not appear to have this field sanitized
    published_date = models.CharField(max_length=24, null=True)
    authors = models.ManyToManyField(Author)
    title = models.CharField(max_length=256)
    description = models.TextField(null=True)

    def image_links_default():
        return {
            'thumbnail': 'https://via.placeholder.com/128x190.png?text=No%20Picture'
        }
    # not queried against, only utilized by front end. in the real world I would work around this better and not have a URL default
    image_links = models.JSONField(default=image_links_default)
    # did not want to do this, unsure of how to get past integrity constraint issues in pytest
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True)

    @classmethod
    def create_from_google_books_api(cls, volume: dict):
        volumeInfo = volume['volumeInfo']
        publisher, _ = Publisher.objects.get_or_create(
            name=volumeInfo['publisher'])
        book = cls(
            id=volume['id'],
            publisher=publisher,
            title=volumeInfo['title'],
        )
        if 'imageLinks' in volumeInfo:
            book.image_links = volumeInfo['imageLinks']
        if 'publishedDate' in volumeInfo:
            book.published_date = volumeInfo['publishedDate']
        if 'description' in volumeInfo:
            book.description = volumeInfo['description'],
        book.save()
        if 'authors' in volumeInfo:
            for author_name in volumeInfo['authors']:
                author, _ = Author.objects.get_or_create(name=author_name)
                book.authors.add(author)
        return book

    @classmethod
    def find_or_create(cls, book_id: str):
        try:
            return Book.objects.get(id=book_id)
        except Book.DoesNotExist:
            google_books = GoogleBooks()
            volume = google_books.volume(book_id)
            if 'error' in volume:
                raise Book.DoesNotExist()
            return Book.create_from_google_books_api(volume)


class OktaUser(AbstractUser):
    uid = models.CharField(primary_key=True, max_length=64)
    username = models.CharField(max_length=128, unique=True)
    books = models.ManyToManyField(Book)
